# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 16:07:36 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-



import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
from Org import OrganisationExportMBR
from faker import Faker
from datetime import timedelta
from datetime import date
from datetime import datetime
from faker import Faker





# read the Main file
def read_generated_tables(file_name,my_sheet):
    generated_tables_df = read_excel(file_name, my_sheet)
    #print(generated_tables)
    list_of_tables=generated_tables_df['GenerateTable'].values.tolist()
    return list_of_tables
def number_of_rows(table):
        file_name = 'TestDatav4.xlsx'
        my_sheet = 'Main'
    
        df = read_excel(file_name, my_sheet)
        number=df.loc[df.GenerateTable == table, 'NumberOfRows']
        return number.values[0]
    
# read the table
def read_table(file_name,my_sheet):
    table_list=[]
    try:
        table_df = read_excel(file_name, my_sheet,skiprows=12)
        table_list=table_df.values.tolist()

    except ValueError:
        print(my_sheet," is not found, please correct the table's name")
        
    
    return table_list

def get_columns(file_name,my_sheet):
    name_of_columns= read_excel(file_name, my_sheet,skiprows=12).columns.tolist()
    return name_of_columns
    

def insert_data(db_table_nm,columns_list):

    value_q_list=[]
    for i in range(len(columns_list)):
        value_q_list.append('?')
    
    string_columns_list = ','.join(columns_list)
    string_q_list = ','.join(value_q_list)
    qry= 'INSERT INTO '+ db_table_nm +'('+ string_columns_list +') VALUES ('+ string_q_list+')'
    return qry


# generate data
def generate_data(table,columns,number,tables):
    fake_data = Faker('en_AU')
    final_list=[]
    
    # Address
    if table == 'AddressExportMBR':
        isn=list(range(3000000000,3000000000+number))
        addressno = generate_unique_address('AddressExportMBR')
        for i in range(number):
            addressRows=[isn[i],
               addressno[i],
               fake_data.street_address(),
               fake_data.city(),
               randint(1,999),
               '',	
               fake_data.street_name(),
               '',
               fake_data.postcode(),
               fake_data.state_abbr(),
               'Australia',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               ''] 
            final_list.append(addressRows)
    
    # Person table
    if table == 'PersonExportMBR':
        personno = list(range(1,1+number))
        addressno = generate_unique_address('AddressExportMBR')
        isn=list(range(4000000000,4000000000+number))
        
        table_found = False
        for t in tables:
            if t== 'PersonExportMBR':
                break;
            elif t == 'AddressExportMBR':
                table_found=True
                
        if (table_found == True):
                     
            for i in range(number):
                 personRows=[isn[i],
                           personno[i],
                           fake_data.last_name(),
                           fake_data.first_name(),
                           '',
                           '', 
                           str(fake_data.date_of_birth(tzinfo=None, minimum_age=0, maximum_age=115)).replace('-',''),
                           '',
                           fake_data.state(),
                           fake_data.country(),
                           fake_data.country_code()	,
                           random.choice(addressno),
                           str(fake_data.date(pattern="%Y-%m-%d", end_datetime=None)),
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '']
                 final_list.append(personRows)

        else:
            print(" AddressExportMBR need to be inserted before PersonExporttMBR" )
        
    
    # Org table
    if table == 'OrgExportMBR':
        df= read_excel('TestDatav4.xlsx', 'config_org')
        final = []
        final_result_df = []
        outer_index = 0
        fake_data = Faker('en_AU') 
        
        # replace the nan values
        df1 = df.replace(np.nan, '', regex=True)
        
        # get total number of records
        org_total_number = df1['Number'].sum()

        # grouping the scenarios
        orgNo_list=df1['Senario'].values.tolist()
        grouped = df1.groupby(df1.Senario)
        grouped_list=[]
        new_grouped_list=[]
        acn_list = get_acn(org_total_number)
        abn_list = get_abn(org_total_number)

    
        for i in range(len(orgNo_list)):
            a = grouped.get_group(orgNo_list[i])
            new = a[['Senario','Number','orgType','orgStatus']].values.tolist()
            grouped_list.append(new)
    
        for elem in grouped_list:
            if elem not in new_grouped_list:
                new_grouped_list.append(elem)
        grouped_list = new_grouped_list
        print('Senarios are: ',grouped_list)
        for senario in grouped_list:
            if senario[0][1] == 0:
                continue
            sen,index = create_senario_org(senario,acn_list,abn_list,outer_index)
            final.append(sen)
            outer_index = index
        final_result_df = pd.concat(final,ignore_index=True)
        final_list=final_result_df.values.tolist()
        
        
    return final_list
def create_senario_org(senario,acn_list,abn_list,index):
        
    fake_data = Faker('en_AU') 
    status=[]
    frame=[]
    for s in senario:
        status.append(s[3])
   
    new_number=senario[0][1]
    for i in range(new_number):
            company_name =fake_data.company()
            orgNo = acn_list[index]
            abn = abn_list[index]
            orgType = senario[0][2]
            if orgType == '' :
                 OrganisationClass = ''
                 OrganisationSubClass = ''
            else:
                stat,cl,sub_cl = define_stat_class_subclass(orgType)
                OrganisationClass=random.choice(cl)
                OrganisationSubClass=random.choice(sub_cl)
                
            start_date,end_date = start_end_date(status)
            start_date_reg = [start_date[0].strftime("%Y-%m-%d"), fake_data.date_between_dates(date_start = start_date[0] -timedelta(days=365) , date_end = start_date[0]).strftime("%Y-%m-%d")]
            reg_start_date = random.choice(start_date_reg)
            placeOFInc = fake_data.state_abbr()
            stateOfInc = [placeOFInc , '']

            
            fake_org = [{'OrgNo':orgNo,
                         'ABN':abn,
                         'OrganisationName':company_name,
                             'OrganisationType':orgType,
                             'OrganisationStatus':status[i],
                             'OrganisationClass':OrganisationClass,
                             'OrganisationSubClass':OrganisationSubClass,
                             'PrincipalActivity':'WHOLESALE DISTRIBUTOR' ,
                             'RecordStartDate': start_date[i].strftime("%Y-%m-%d"),#random.choice([fake_data.date_between_dates(date_start = start_date[i], date_end = start_date[i]+pd.DateOffset(years=1)).strftime("%Y-%m-%d"),start_date[i].strftime("%Y-%m-%d")]),
                             'RecordEndDate':end_date[i].strftime("%Y-%m-%d"),
                             'NameStartDate':reg_start_date,
                             'RegistrationStartDate':reg_start_date,
                             'RegistrationEndDate':end_date[i].strftime("%Y-%m-%d"),
                             'DeregnReasonCode':'',
                             'LastBalanceDate': '',
                             'StateOfIncorporation' : random.choice(stateOfInc),
                             'PlaceOfIncorporation' : placeOFInc,
                             'DisclosingEntityFlag':'',
                             'SuperTrustCoyFlag': '',
                             'PreviousStateofRegn':'',
                             'PreviousStateRegnNo':'',
                             'NextReviewDDMM'	:reg_start_date[-5:].replace('-',''),
                             'UpdateDocNo':'',
                             'CreateDocNo':'',
                             'AmendInd':'',
                             'FormerNameFlag':'',
                             'NextReviewDate':'',
                             'NonASICOrgFlag':'',
                             'OrgNameID' :'',
                             'SuccessorOrgNo':'',
                             'FirstBalanceDate':'',
                             'PreviousOrgSubClass':'',
                             'CreationDate':'',
                             'OMD_OPERATOR_ID':''} for i in range(len(status))] 
            index=index+1
        
            fake_org_df = pd.DataFrame(fake_org)

            
            fake_org_df['RegistrationEndDate']= np.where(fake_org_df['OrganisationStatus'] == 'DREGD', fake_org_df['RecordStartDate'],'9999-12-31')
            fake_org_df['DeregnReasonCode']= np.where(fake_org_df['OrganisationStatus'] == 'DREGD', 'S601AA','')
            frame.append(fake_org_df)

    result =  pd.concat(frame,ignore_index=True)
    return result,index
def start_end_date(status):
    fake = Faker('en_AU')
    start = fake.date_between(start_date="-50y", end_date="today")
    start_dates = []
    end_dates = []
    start_dates.append(start)
    for i in range(len(status)):
        if i != len(status)-1:
            end = fake.date_between_dates(date_start=start_dates[i], date_end=None)
            end_dates.append(end)
            next_start = end + timedelta(days=1)
            start_dates.append(next_start)
        else:
           a = datetime.strptime('9999-12-31','%Y-%m-%d')
           end_dates.append(a)

    return start_dates,end_dates
def define_stat_class_subclass(org_type):
        status = []
        cl = []
        sub_cl = []
        
        if(org_type != ''):
            df= read_excel('TestDatav4.xlsx', 'Config')
            df1= df[['OrganisationType', 'OrganisationStatus','OrganisationClass' ,'OrganisationSubClass']]
            istrue=df1['OrganisationType']== org_type
            new_df = df1[istrue]
            stat = []
            org_class = []
            org_subclass = []
            for index, row in new_df.iterrows():
                orgType= row['OrganisationType'] 
                orgStat =row['OrganisationStatus']
                orgClass= row['OrganisationClass']
                orgsubClass= row['OrganisationSubClass']
                stat.append(orgStat)
                org_class.append(orgClass)
                org_subclass.append(orgsubClass)
                
                
                
            status=list(dict.fromkeys(stat))
            cl=list(dict.fromkeys(org_class))
            sub_cl=list(dict.fromkeys(org_subclass))
        
        
        return status,cl ,sub_cl 

def generate_unique_address(table):
    number = number_of_rows(table)
    addressno = list(range(100,100+number))
    return addressno
def get_acn(number):
    df= read_excel('TestDatav4.xlsx', 'acn-abn')
    acn_list = df['ACN'].values.tolist()
    final_acn_list =[]
    for i in range(number):
       final_acn_list.append(acn_list[i])
    return final_acn_list
    
def get_abn(number):
    df= read_excel('TestDatav4.xlsx', 'acn-abn')
    abn_list = df['ABN'].values.tolist()
    final_abn_list =[]
    for i in range(number):
       final_abn_list.append(abn_list[i])
    return final_abn_list

         
    


# print the tables for inserting data
tables=read_generated_tables('TestDatav4.xlsx','Main')
print(tables)


# print the data for table
for table in tables:
    
    
    print("table name is", table)
    number_of_row = number_of_rows(table)
    data = read_table('TestDatav4.xlsx',table)
    columns = get_columns('TestDatav4.xlsx',table)
    ls = generate_data(table,columns,number_of_row,tables)
    lst_tuple =list(map(tuple, ls))


    q = insert_data(table,columns)
  
    print('The number of rows are:',number_of_row)
    print(q)
    print(lst_tuple)
   
    with open(table+'.txt' , 'w') as f:
         f.write(q+"\n")
         f.write("\n")
         for l in lst_tuple :
           f.write(str(l) +"\n")
    



