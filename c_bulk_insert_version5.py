# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 16:41:04 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 16:44:24 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-


import pyodbc
import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
import string
from Address import AddressExportMBR
from Person import PersonExportMBR
from Org import OrganisationExportMBR


def read_input_acn(file_name,my_sheet):
    acn_df = read_excel(file_name, my_sheet)
    #print(generated_tables)
    acn_list=acn_df['ACN'].values.tolist()
    return acn_list

def random_string_generator(str_size, allowed_chars):
    s=''.join(random.choice(allowed_chars) for x in range(str_size))
    new_string=s+ ' PTY LTD'
    return new_string

# read the Main file
def read_generated_tables(file_name,my_sheet):
    generated_tables_df = read_excel(file_name, my_sheet)
    #print(generated_tables)
    list_of_tables=generated_tables_df['GenerateTable'].values.tolist()
    return list_of_tables
def number_of_rows(table):
        file_name = 'TestDatav4.xlsx'
        my_sheet = 'Main'
    
        df = read_excel(file_name, my_sheet)
        number=df.loc[df.GenerateTable == table, 'NumberOfRows']
        return number.values[0]
    
# read the table
def read_table(file_name,my_sheet):
    table_list=[]
    try:
        table_df = read_excel(file_name, my_sheet,skiprows=12)
        table_list=table_df.values.tolist()

    except ValueError:
        print(my_sheet," is not found, please correct the table's name")
        
    
    return table_list

def get_columns(file_name,my_sheet):
    name_of_columns= read_excel(file_name, my_sheet,skiprows=12).columns.tolist()
    return name_of_columns
    

def insert_data(db_table_nm,columns_list,data):

    value_q_list=[]
    for i in range(len(columns_list)):
        value_q_list.append('?')
    
    string_columns_list = ','.join(columns_list)
    string_q_list = ','.join(value_q_list)
    qry= 'INSERT INTO '+ db_table_nm +'('+ string_columns_list +') VALUES ('+ string_q_list+')'
    return qry


# generate data
def generate_data(table,data,columns,number):
    
    personno = list(range(1,1+number))    
    final_list=[]

    if table == 'AddressExportMBR':
        isn=list(range(3000000000,3000000000+number))
        addressno = generate_unique_address('AddressExportMBR')

        for i in range(number):
            address_record_obj= AddressExportMBR(isn[i],addressno[i])
            address_record_list=address_record_obj.get_data()
            final_list.append(address_record_list)
            
        
    if table == 'PersonExportMBR':
            isn=list(range(4000000000,4000000000+number))
            address_list=generate_unique_address('AddressExportMBR')
            df_name = pd.read_excel('Names.xlsx', 'name')
            firstname_list = df_name['Name'].values.tolist()
            surname_list = df_name['Surname'].values.tolist()
            for i in range(number):
                person_record_obj = PersonExportMBR(isn[i],personno[i],address_list,firstname_list,surname_list)
                person_record_list = person_record_obj.get_data()
                final_list.append(person_record_list)
    """
         tables=read_generated_tables('TestDatav4.xlsx','Main')
         
         table_found=False 
         
         for table in tables:
             if table == 'PersonExportMBR':
                 break
             elif table == 'AddressExportMBR':
                 table_found = True
         if(table_found == True):
            
            isn=list(range(4000000000,4000000000+number))
            
            for i in range(number):
                person_record_obj = PersonExportMBR(isn[i],personno[i],generate_unique_address('AddressExportMBR'))
                person_record_list = person_record_obj.get_data()
                final_list.append(person_record_list)
         else:
            print(" AddressExportMBR need to be inserted before PersonExporttMBR" )
    """
    #org table
    if table == 'OrgExportMBR':
        df= read_excel('TestDatav4.xlsx', 'config_org')
        df1=df[['Senario','Number','orgType','orgStatus']]
        number_list = df[['Senario','Number']].values.tolist()
        
        new_number_list=[]
        org_total_number=0
        for n in number_list:
            if n not in new_number_list:
                new_number_list.append(n)
        for i in range(len(new_number_list)):
            org_total_number=new_number_list[i][1]+org_total_number
        

        orgNo_list=df1['Senario'].values.tolist()
        grouped = df1.groupby(df1.Senario)
        grouped_list=[]
        new_grouped_list=[]
        #unique_org=df['Senario'].values.tolist()
        #number_of_org = len(list(dict.fromkeys(unique_org)))
        acn_list = get_acn(org_total_number)
        abn_list = get_abn(org_total_number)
        index = 0
        org_elements=[]
        final = []

    
        for i in range(len(orgNo_list)):
            a = grouped.get_group(orgNo_list[i])
            new = a[['Senario','Number','orgType','orgStatus']].values.tolist()
            grouped_list.append(new)
        
    
        for elem in grouped_list:
            if elem not in new_grouped_list:
                new_grouped_list.append(elem)
        grouped_list = new_grouped_list
        
    
        for m in grouped_list:
            new_number=m[0][1]  
            for i in range(int(new_number)):      
                obj = OrganisationExportMBR(acn_list[index],abn_list[index],m)
                result = obj.getData()
                index =index+1
                org_elements.append(result)

     
        for element in org_elements:
            for i in range(len(element)):
                final.append(element[i])
    
        isn=list(range(1000000000,1000000000+len(final)))
        last_list=[]
        for i in range(len(final)):
            my_list=final[i]
            my_list[:0] = [isn[i]]
            last_list.append(my_list)
        final_list=last_list
  
    return final_list
def generate_unique_address(table):
    number = number_of_rows(table)
    addressno = list(range(100,100+number))
    return addressno
def get_acn(number):
    df= read_excel('TestDatav4.xlsx', 'Config')
    acn_list = df['ACN'].values.tolist()
    final_acn_list =[]
    for i in range(number):
       final_acn_list.append(acn_list[i])
    return final_acn_list
    
def get_abn(number):
    df= read_excel('TestDatav4.xlsx', 'Config')
    abn_list = df['ABN'].values.tolist()
    final_abn_list =[]
    for i in range(number):
       final_abn_list.append(abn_list[i])
    return final_abn_list

         
    


# print the tables for inserting data
tables=read_generated_tables('TestDatav4.xlsx','Main')
print(tables)


# print the data for table
for table in tables:
    
    
    print("table name is", table)
    number_of_row = number_of_rows(table)
    data = read_table('TestDatav4.xlsx',table)
    columns = get_columns('TestDatav4.xlsx',table)
    ls = generate_data(table,data,columns,number_of_row)
    lst_tuple =list(map(tuple, ls))


    q = insert_data(table,columns,data)
  
    print('The number of rows are:',number_of_row)
    print(q)
    print(lst_tuple)
   
    with open(table+'.txt' , 'w') as f:
         f.write(q+"\n")
         f.write("\n")
         for l in lst_tuple :
           f.write(str(l) +"\n")
    



