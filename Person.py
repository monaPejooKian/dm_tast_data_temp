# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 12:30:42 2021

@author: mona.abedi
"""
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 12:12:38 2021

@author: mona.abedi
"""
import pyodbc
import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
import string
import datetime
import random
from random import randrange
from datetime import timedelta
from datetime import date
from datetime import datetime

class PersonExportMBR:

    def __init__(self,ISN,person_no,address_no=[]):
        
       df_name = pd.read_excel('Names.xlsx', 'name')
       firstname_list = df_name['Name'].values.tolist()
       surname_list = df_name['Surname'].values.tolist()
       self.address_no=address_no
       self.ISN=ISN
       self.PersonNo = person_no
       self.Surname = random.choice(surname_list)
       self.GivenName1 = random.choice(firstname_list)
       self.GivenName2 = ''
       self.GivenName3 = ''
       self.BirthDate = self.generate_date()
       self.BirthLocality=''
       self.BirthState=''
       self.BirthCountry ='Australia'
       self.	BirthCountryCode	 = 123
       self.AddressNo = random.choice(self.address_no)
       self.PersonStartDate = self.generate_date()
       self.PersonEndDate = self.greater_date(self.PersonStartDate,'20210823')
       self.RecordStartDate = self.greater_date(self.PersonStartDate,self.PersonEndDate)
       self.RecordEndDate = self.PersonEndDate
       self.ApprovalNo = ''
       self.ABN =''
       self.UpdateDocNo =''
       self.CreateDocNo =''
       self.FormerNameFlag =''
       self.Amendindicator =''
       self.MD_OPERATOR_ID = ''
       
    def get_data(self):
        l=[self.ISN,
           self.PersonNo,
           self.Surname ,
           self.GivenName1, 
           self.GivenName2, 
           self.GivenName3, 
           self.BirthDate,
           self.BirthLocality,
           self.BirthState,
           self.BirthCountry,
           self.	BirthCountryCode	,
           self.AddressNo ,
           self.PersonStartDate,
           self.PersonEndDate,
           self.RecordStartDate,
           self.RecordEndDate,
           self.ApprovalNo ,
           self.ABN,
           self.UpdateDocNo,
           self.CreateDocNo,
           self.FormerNameFlag,
           self.Amendindicator,
           self.MD_OPERATOR_ID]
        return l
    
    def random_string_generator_addressLine1(self,str_size, allowed_chars):
        s=''.join(random.choice(allowed_chars) for x in range(str_size))
        new_string=s +' '+ 'st'
        return new_string
    def random_string_generator_addressLine2(self,str_size, allowed_chars):
        s=''.join(random.choice(allowed_chars) for x in range(str_size))
        new_string=s +' '
        return new_string
    def generate_date(self):
        year=str(random.randint(1900,2020))
        month=str(random.randint(1,12)).zfill(2)
        day=str(random.randint(1,30)).zfill(2)
        b_date = year + month + day
        return b_date
    def greater_date(self,start_date,end_date):
        d1 = datetime.strptime(start_date, "%Y%m%d")
        d2 = datetime.strptime(end_date, "%Y%m%d")
        return self.random_date(d1,d2)
    
    def random_date(self,start,end):
        
        delta = end - start
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = randrange(int_delta)
        a=str(start + timedelta(seconds=random_second))
        result = a[:10]
        return result.replace('-', '')




























