# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 18:38:52 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 28 20:10:58 2021

@author: mona.abedi
"""
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 13:07:43 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 12:12:38 2021

@author: mona.abedi
"""
import pyodbc
import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
import string
import pyodbc
import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
import string
import datetime
import random
from random import randrange
from datetime import timedelta
from datetime import date
from datetime import datetime


class OrganisationExportMBR:

    def __init__(self,acn,abn,m=[]):
        
        self.stats=[]
        for i in range(len(m)):
            self.stats.append(m[i][3])
        self.OrganisationType=m[0][2]

            
       
        state =['VIC','NSW','ACT','NT','QLD','SA']
        #self.ISN = ISN	
        self.OrgNo = str(acn)
        self.ABN	= str(abn)
     
        self.OrganisationName = self.random_string_generator(12,string.ascii_letters,self.OrganisationType)
        stat,cl,sub_cl = self.define_stat_class_subclass(self.OrganisationType)
        self.OrganisationStatus = self.stats
        self.OrganisationClass=random.choice(cl)
        self.OrganisationSubClass=random.choice(sub_cl)
        self.PrincipalActivity = 'WHOLESALE DISTRIBUTOR'
        
        self.RegistrationStartDate =	self.generate_date()
        self.RegistrationEndDate	=''
        recstart_list =[]
        recstart_list.append(self.RegistrationStartDate)
        recstart_list.append('20210829')
        #recstart_list.append(self.greater_date(self.RegistrationStartDate,'20210829'))
        self.ReordStartDate	= random.choice(recstart_list)
        self.NameStartDate = self.ReordStartDate	
        self.RecordEndDate	= ''
        self.DeregnReasonCode =''
        self.LastBalanceDate	= ''
        self.StateOfIncorporation= random.choice(state)
        self.PlaceOfIncorporation =random.choice(state)
        self.DisclosingEntityFlag =''
        self.SuperTrustCoyFlag= ''
        self.PreviousStateofRegn	=''
        self.PreviousStateRegnNo	=''
        self.NextReviewDDMM	=''
        self.UpdateDocNo=''
        self.CreateDocNo	=''
        self.AmendInd=''
        self.FormerNameFlag=''
        self.NextReviewDate=self.greater_date(self.RegistrationStartDate,'20210829')
        self.NonASICOrgFlag=''	
        self.OrgNameID =''
        self.SuccessorOrgNo=''
        self.FirstBalanceDate=''
        self.PreviousOrgSubClass=''
        self.CreationDate=''
        self.OMD_OPERATOR_ID=''

    def getData(self):

        l=[]
        for i in range(len(self.OrganisationStatus)):

            l.append([
            self.OrgNo,
            self.ABN,
            self.OrganisationName,
            self.OrganisationType,
            self.OrganisationStatus[i],
            self.OrganisationClass,
            self.OrganisationSubClass,
            self.PrincipalActivity,
            self.ReordStartDate,
            self.RecordEndDate,
            self.NameStartDate ,
            self.RegistrationStartDate ,
            self.RegistrationEndDate,
            self.DeregnReasonCode,
            self.LastBalanceDate	,
            self.StateOfIncorporation,
            self.PlaceOfIncorporation,
            self.DisclosingEntityFlag,
            self.SuperTrustCoyFlag,
            self.PreviousStateofRegn,
            self.PreviousStateRegnNo	,
            self.NextReviewDDMM,
            self.UpdateDocNo,
            self.CreateDocNo,
            self.AmendInd,
            self.FormerNameFlag,
            self.NextReviewDate,	
            self.OrgNameID ,
            self.SuccessorOrgNo,
            self.FirstBalanceDate,
            self.PreviousOrgSubClass,
            self.CreationDate,
            self.OMD_OPERATOR_ID])
            
        return l

    
    def random_string_generator(self,str_size, allowed_chars,org_type):
        s=''.join(random.choice(allowed_chars) for x in range(str_size))
        
        
        if org_type == 'APTY':
            new_string=s+ ' PTY LTD'
            
        elif org_type == 'APUB':
            new_string=s+ ' LTD'
        else:
            new_string=s
        return new_string

    def generate_date(self):
        year=str(random.randint(1900,2020))
        month=str(random.randint(1,12)).zfill(2)
        day=str(random.randint(1,30)).zfill(2)
        b_date = year + month + day
        return b_date
    def greater_date(self,start_date,end_date):
        d1 = datetime.strptime(start_date, "%Y%m%d")
        d2 = datetime.strptime(end_date, "%Y%m%d")
        return self.random_date(d1,d2)

    def random_date(self,start,end):
        
        delta = end - start
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = randrange(int_delta)
        a=str(start + timedelta(seconds=random_second))
        result = a[:10]
        return result.replace('-', '')
    def define_stat_class_subclass(self,org_type):
        df= read_excel('TestDatav4.xlsx', 'Config')
        df1= df[['OrganisationType', 'OrganisationStatus','OrganisationClass' ,'OrganisationSubClass']]
        istrue=df1['OrganisationType']== org_type
        new_df = df1[istrue]
        stat = []
        org_class = []
        org_subclass = []
        for index, row in new_df.iterrows():
            orgType= row['OrganisationType'] 
            orgStat =row['OrganisationStatus']
            orgClass= row['OrganisationClass']
            orgsubClass= row['OrganisationSubClass']
            stat.append(orgStat)
            org_class.append(orgClass)
            org_subclass.append(orgsubClass)
            
            
            
        status=list(dict.fromkeys(stat))
        cl=list(dict.fromkeys(org_class))
        sub_cl=list(dict.fromkeys(org_subclass))
        
        
        return status,cl ,sub_cl 
    """
    def list_for_enddate(self,RegistrationStartDate):
        end=[]
        a=self.greater_date(RegistrationStartDate,'20210823')
        a.append(end)
        a.append.('')
        return end
     """   


    
    
    
    
    
    
    