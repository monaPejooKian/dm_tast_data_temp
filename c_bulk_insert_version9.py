# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 17:07:29 2021

@author: mona.abedi
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 11:33:43 2021

@author: mona.abedi -v8
"""




import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
from Org import OrganisationExportMBR
from faker import Faker
from datetime import timedelta
from datetime import date
from datetime import datetime
from faker import Faker





# read the Main file
def read_generated_tables(file_name,my_sheet):
    generated_tables_df = read_excel(file_name, my_sheet)
    #print(generated_tables)
    list_of_tables=generated_tables_df['GenerateTable'].values.tolist()
    return list_of_tables
def number_of_rows(table):
        file_name = 'TestDatav4.xlsx'
        my_sheet = 'Main'
    
        df = read_excel(file_name, my_sheet)
        number=df.loc[df.GenerateTable == table, 'NumberOfRows']
        return number.values[0]
    
# read the table
def read_table(file_name,my_sheet):
    table_list=[]
    try:
        table_df = read_excel(file_name, my_sheet,skiprows=12)
        table_list=table_df.values.tolist()

    except ValueError:
        print(my_sheet," is not found, please correct the table's name")
        
    
    return table_list

def get_columns(file_name,my_sheet):
    name_of_columns= read_excel(file_name, my_sheet,skiprows=12).columns.tolist()
    return name_of_columns
    

def insert_data(db_table_nm,columns_list):

    value_q_list=[]
    for i in range(len(columns_list)):
        value_q_list.append('?')
    
    string_columns_list = ','.join(columns_list)
    string_q_list = ','.join(value_q_list)
    qry= 'INSERT INTO '+ db_table_nm +'('+ string_columns_list +') VALUES ('+ string_q_list+')'
    return qry


# Generate data for Address Table
def address_func(table,columns,number,tables):

    fake_data = Faker('en_AU')
    num_aus_address=int((number*2)/3)
    addressno = generate_unique_address('AddressExportMBR')

    fake_address_aus = [{'AddressNo' :	addressno[i],
                     'AddressLine1'	:fake_data.street_address(),
                     'AddressLine2'	:fake_data.city(),
                     'StreetNo'	:randint(1,999),
                     'StreetType':	'',
                     'StreetName':fake_data.street_name(),
                     'Locality'	:'',
                     'PostCode'	:fake_data.postcode(),
                     'State':fake_data.state_abbr(),
                     'Country'	: 'Australia',
                     'AmendIndicator':	'',
                     'UpdateDocNo'	: '',
                     'CreatedDocNo'	:'',
                     'DPID'	:'',
                     'Form379AddressNo':	'',
                     'CreatedDate'	:'',
                     'ForeignAddressLine1':'',
                     'ForeignAddressLine2':'',
                     'ForeignAddressLine3'	:'',
                     'ForeignAddressLine4'	:'',
                     'OMD_INSERT_DATETIME'	:'',
                     'OMD_OPERATOR_ID':''}for i in range(num_aus_address)]
    fake_address_fnos = [{'AddressNo' :	addressno[i],
                 'AddressLine1'	:'',
                 'AddressLine2'	:'',
                 'StreetNo'	:'',
                 'StreetType':	'',
                 'StreetName':'',
                 'Locality'	:'',
                 'PostCode'	:'',
                 'State':'',
                 'Country'	: Faker().country(),
                 'AmendIndicator':	'',
                 'UpdateDocNo'	: '',
                 'CreatedDocNo'	:'',
                 'DPID'	:'',
                 'Form379AddressNo':	'',
                 'CreatedDate'	:'',
                 'ForeignAddressLine1':Faker().address(),
                 'ForeignAddressLine2':'',
                 'ForeignAddressLine3'	:'',
                 'ForeignAddressLine4'	:'',
                 'OMD_INSERT_DATETIME'	:'',
                 'OMD_OPERATOR_ID':''}for i in range(num_aus_address+1, number)]
    fake_address_data_aus = pd.DataFrame(fake_address_aus)
    fake_address_data_fnos= pd.DataFrame(fake_address_fnos)
    frames= [fake_address_data_aus,fake_address_data_fnos]
    result = pd.concat(frames)
    final_result = result.values.tolist()
    

   # fake_address_data.loc[0:num_aus_address,['AddressLine1','AddressLine2','StreetNo','StreetName','PostCode','State','Country']] = [fake_data.street_address(),fake_data.city(),randint(1,100000),fake_data.street_name(),fake_data.postcode(),fake_data.state_abbr(),'Australia']
    #fake_address_data.loc[num_aus_address+1:number,['ForeignAddressLine1']] = [Faker().address()]
    #result = fake_address_data.values.tolist()

    return final_result

# Generate data for Person Table
def person_func(table,columns,number,tables):
    fake_data = Faker('en_AU')
    final_list = []
    personno = list(range(1,1+number))
    addressno = generate_unique_address('AddressExportMBR')
    #isn=list(range(4000000000,4000000000+number))
    
    table_found = False
    for t in tables:
        if t== 'PersonExportMBR':
            break;
        elif t == 'AddressExportMBR':
            table_found=True
            
    if (table_found == True):
                     
            for i in range(number):
                 personRows=[
                           personno[i],
                           fake_data.last_name(),
                           fake_data.first_name(),
                           '',
                           '', 
                           str(fake_data.date_of_birth(tzinfo=None, minimum_age=0, maximum_age=115)).replace('-',''),
                           '',
                           fake_data.state(),
                           fake_data.country(),
                           fake_data.country_code()	,
                           random.choice(addressno),
                           str(fake_data.date(pattern="%Y-%m-%d", end_datetime=None)),
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '',
                           '']
                 final_list.append(personRows)

    else:
            print(" AddressExportMBR need to be inserted before PersonExporttMBR" )
        
    return final_list
# Generate data for Org Table 
def org_func(table,columns,number,tables):
    final_list = []
    fake_data = Faker('en_AU')
    df= read_excel('TestDatav4.xlsx', 'config_org')
    final = []
    final_result_df = []
    outer_index = 0
 
    
    # replace the nan values
    df1 = df.replace(np.nan, '', regex=True)
    
    # get total number of records
    org_total_number = df1['Number'].sum()

    # grouping the scenarios
    orgNo_list=df1['Senario'].values.tolist()
    grouped = df1.groupby(df1.Senario)
    grouped_list=[]
    new_grouped_list=[]
    acn_list = get_acn(org_total_number)
    abn_list = get_abn(org_total_number)


    for i in range(len(orgNo_list)):
        a = grouped.get_group(orgNo_list[i])
        new = a[['Senario','Number','orgType','orgStatus']].values.tolist()
        grouped_list.append(new)

    for elem in grouped_list:
        if elem not in new_grouped_list:
            new_grouped_list.append(elem)
    grouped_list = new_grouped_list
    print('Senarios are: ',grouped_list)
    for senario in grouped_list:
        if senario[0][1] == 0:
            continue
        sen,index = create_senario_org(senario,acn_list,abn_list,outer_index)
        final.append(sen)
        outer_index = index
    final_result_df = pd.concat(final,ignore_index=True)
   # df_orgNo_orgType = final_result_df[['OrgNo','OrganisationType']]
    final_list = final_result_df.values.tolist()

    return final_list
def start_end_date_role(role_length):
    fake = Faker('en_AU')
    #start = fake.date_between(start_date="-40y", end_date="today")
    start = fake.date_between_dates(date_start=datetime(1970,1,1), date_end=datetime(2021,5,31))
    start_dates = []
    end_dates = []
    start_dates.append(start)
    for i in range(role_length):
        if i != role_length-1:
            #print('test11111111111111111111', start_dates[i])
           # print('test222222222222222222', date.today())
            end = fake.date_between_dates(date_start=start_dates[i], date_end=date.today())
            #while end == date.today():
                #end = fake.date_between_dates(date_start=start_dates[i], date_end=date.today())
                         
            end_dates.append(end)
            next_start = end + timedelta(days=1)
            start_dates.append(next_start)
        else:
           a = datetime.strptime('9999-12-31','%Y-%m-%d')
           end_dates.append(a)

    return start_dates,end_dates
    
def create_role(role,acn_list,ind,person_no,sec_person_no,sec_start_date_list,sec_end_date_list,sec_address_no,current_address):


    #number of record for each role for each sceanrio
    history_role = role[3]
    role_start_date,role_end_date = start_end_date_role(history_role)
    role_name = role[2]
    frame_role =[]

    #address type Role
    if role[4] == 1 and role[5] == 0 and role[6] == 0 and role[7] == 0 and role[8] == 0:     
        addressno = generate_unique_address('AddressExportMBR')
        number=len(addressno)
        if role_name == 'RO':
            addressno_data = addressno[int((number*2)/3)+1:number]
        else:
            addressno_data = addressno[0:int((number*2)/3)]
    
        fake_role = [{'OwnerOrgNo':	acn_list[ind],
                      'OwnerPersonNo':'',
                      'MemberOrgNo'	:'',
                      'MemberPersonNo'	:'',
                      'Role':role_name,
                      'RoleStartDate': role_start_date[i].strftime("%Y-%m-%d"),
                      'RoleEndDate':role_end_date[i].strftime("%Y-%m-%d"),
                      'AddressNo':	np.random.choice(addressno_data,replace=True),
                      'UpdateDocNo':''	,
                      'CreateDocNo':''	,
                      'RefusalReason':	'',
                      'StrikeOffReason':	'',
                      'SuspensionReason':'',
                      'AplicationStatus':	'' ,
                      'ApplicationNo':	'',
                      'RecordStartDate':	role_start_date[i].strftime("%Y-%m-%d"),
                      'RecordEndDate':'9999-12-31',
                      'PreviousLicenceNo':	'',
                      'ExpiryDate':	'',
                      'AmendIndicator':'',
                      'CompletionUpdateDocNo': ''	,
                      'CompletionUpdateDate':	'',
                      'DisqualificationUpdateDocNo'	:'',
                      'DisqualificationEndDate':	'',
                      'ResignationUpdateDocNo':	'',
                      'ResignationEndDate':	'',
                      'ConsentWithdrawnFlag':	'',
                      'ExtendedText':	'',
                      'OMD_INSERT_DATETIME'	:'',
                      'OMD_OPERATOR_ID':'' }for i in range(history_role)]
        fake_role_type = pd.DataFrame(fake_role)


        frame_role.append(fake_role_type)
        
    # individual Role
    if role[5] == 1:
        
        if role_name == 'SR' and sec_person_no !=0 and len(sec_start_date_list)!=0 and sec_end_date_list!=0 :
            member_person_no = sec_person_no
            role_start_date = sec_start_date_list
            role_end_date = sec_end_date_list
            addressno = sec_address_no
        else:   
            member_person_no = np.random.choice(person_no,replace=True)
            addressno = generate_unique_address('AddressExportMBR')
        fake_role = [{'OwnerOrgNo':	acn_list[ind],
                      'OwnerPersonNo':'',
                      'MemberOrgNo'	:'',
                      'MemberPersonNo':member_person_no,
                      'Role':role_name,
                      'RoleStartDate': role_start_date[0].strftime("%Y-%m-%d"),
                      'RoleEndDate':'9999-12-31',
                      'AddressNo':	np.random.choice(addressno,replace=True),
                      'UpdateDocNo':''	,
                      'CreateDocNo':''	,
                      'RefusalReason':	'',
                      'StrikeOffReason':	'',
                      'SuspensionReason':'',
                      'AplicationStatus':	'' ,
                      'ApplicationNo':	'',
                      'RecordStartDate':role_start_date[i].strftime("%Y-%m-%d"),
                      'RecordEndDate':role_end_date[i].strftime("%Y-%m-%d"),
                      'PreviousLicenceNo':	'',
                      'ExpiryDate':	'',
                      'AmendIndicator':'',
                      'CompletionUpdateDocNo': ''	,
                      'CompletionUpdateDate':	'',
                      'DisqualificationUpdateDocNo'	:'',
                      'DisqualificationEndDate':	'',
                      'ResignationUpdateDocNo':	'',
                      'ResignationEndDate':	'',
                      'ConsentWithdrawnFlag':	'',
                      'ExtendedText':	'',
                      'OMD_INSERT_DATETIME'	:'',
                      'OMD_OPERATOR_ID':'' }for i in range(history_role)]
        fake_role_type = pd.DataFrame(fake_role)
        fake_role_type.loc[(fake_role_type['RecordEndDate'] == '9999-12-31'), 'AddressNo'] = current_address
        
        # if IsAddressPorulate = 0
        if role[4] == 0:
            fake_role_type['AddressNo'] =''
        
            
        frame_role.append(fake_role_type)
        
    result_role_df = pd.concat(frame_role)    
    return result_role_df

def create_senario_role(senario,acn_type_list,acn_list,index,person_no):

    frame = []
    df_acn_apty_apub_fnos_racn = pd.DataFrame(acn_type_list, columns = ['ACN','Type'])
    df_acn_apty_apub_fnos_racn.drop_duplicates(subset='ACN',keep='first',inplace=True)
    mask = (df_acn_apty_apub_fnos_racn['Type'] == 'FNOS') | (df_acn_apty_apub_fnos_racn['Type'] == 'RACN')
    df_fnos_racn = df_acn_apty_apub_fnos_racn[mask]
    df_apty_apub = df_acn_apty_apub_fnos_racn[~mask]
    #print('public and private orgs are ',df_apty_apub)
    sec_person_no = 0
    sec_start_date_list =[]
    sec_end_date_list = []
    sec_address_no = []
    current_address = 0


    number_of_senario = senario[0][1]
    for i in range(number_of_senario):
        

        for s in senario:
            fake_role = create_role(s,acn_list,index,person_no,sec_person_no,sec_start_date_list,sec_end_date_list,sec_address_no,current_address)
            if fake_role.iloc[0]['Role'] == 'RG':
                current_address = fake_role.iloc[-1]['AddressNo']
            sec_start_date_list.clear()
            sec_end_date_list.clear()   
            if fake_role.iloc[0]['Role'] == 'DR':
                sec_person_no = fake_role.iloc[0]['MemberPersonNo']
                sec_address_no =  fake_role['AddressNo'].values.tolist()
                role_record_end= fake_role['RecordEndDate'].values.tolist()
                role_start= fake_role['RecordStartDate'].values.tolist()
                for i in range(len(role_record_end)):
                   a = datetime.strptime(role_record_end[i],'%Y-%m-%d') 
                   b = datetime.strptime(role_start[i],'%Y-%m-%d') 
                   sec_end_date_list.append(a)
                   sec_start_date_list.append(b)
    
            frame.append(fake_role)
        index=index+1
    
    result_one_scenario =  pd.concat(frame,ignore_index=True)
   

    return result_one_scenario,index


# Generate data for Role Table
def role_func(table,columns,number,tables,acn_type_list,org_acn_list,person_no):
    df= read_excel('TestDatav4.xlsx', 'config_role')
    """
  
    df_address = df[df.ADR == 1]
    df_ind = df[df.IND == 1]
    df_coy = df[df.COY == 1]
    df_org = df[df.ORG == 1]
    df_state = df[df.STATE == 1]
    """

    acn_list = list(dict.fromkeys(org_acn_list))
    df['total'] = df['Scenario Number']*df['History']
    total_number = df['total'].sum()
    
    outer_index = 0
    #fake_data = Faker('en_AU') 
    
    orgNo_list=df['Scenario'].values.tolist()
    grouped = df.groupby(df.Scenario)
    grouped_list=[]
    new_grouped_list=[]
    #acn_list = get_acn(total_number)
    #abn_list = get_abn(total_number)
    outer_index=0
    frame = []
    for i in range(len(orgNo_list)):
        a = grouped.get_group(orgNo_list[i])
        new = a[['Scenario','Scenario Number','Role','History','ADR','IND','COY','ORG','STATE']].values.tolist()
        grouped_list.append(new)
    
    
    for elem in grouped_list:
        if elem not in new_grouped_list:
            new_grouped_list.append(elem)
    grouped_list = new_grouped_list
    print('Senarios are: ',grouped_list)
    
    for sen in grouped_list:
      
        final_senario,index = create_senario_role(sen,acn_type_list,acn_list,outer_index,person_no)
        frame.append(final_senario)
        outer_index=index
    result_all_senario =  pd.concat(frame,ignore_index=True)
    final_list_role = result_all_senario.values.tolist()
    return final_list_role


# generate data
def generate_data(table,columns,number,tables,acn_type,org_acn,person_no):
    fake_data = Faker('en_AU')
    final_list=[]

    
    # Address
    if table == 'AddressExportMBR':
        final_list = address_func(table,columns,number,tables)
   
    # Person table
    if table == 'PersonExportMBR':
       final_list = person_func(table,columns,number,tables)

    # Org table
    if table == 'OrgExportMBR':
        final_list = org_func(table,columns,number,tables)

  
    if table  == 'RoleExportMBR':

        final_list = role_func(table,columns,number,tables,acn_type,org_acn,person_no)
        
    return final_list
def create_senario_org(senario,acn_list,abn_list,index):
        
    fake_data = Faker('en_AU') 
    status=[]
    frame=[]
    for s in senario:
        status.append(s[3])
   
    new_number=senario[0][1]
    for i in range(new_number):
            company_name =fake_data.company()
            orgNo = acn_list[index]
            abn = abn_list[index]
            orgType = senario[0][2]
            if orgType == '' :
                 OrganisationClass = ''
                 OrganisationSubClass = ''
            else:
                stat,cl,sub_cl = define_stat_class_subclass(orgType)
                OrganisationClass=random.choice(cl)
                OrganisationSubClass=random.choice(sub_cl)
                
            start_date,end_date = start_end_date(status)
            start_date_reg = [start_date[0].strftime("%Y-%m-%d"), fake_data.date_between_dates(date_start = start_date[0] -timedelta(days=365) , date_end = start_date[0]).strftime("%Y-%m-%d")]
            reg_start_date = random.choice(start_date_reg)
            placeOFInc = fake_data.state_abbr()
            stateOfInc = [placeOFInc , '']

            
            fake_org = [{'OrgNo':orgNo,
                         'ABN':abn,
                         'OrganisationName':company_name,
                             'OrganisationType':orgType,
                             'OrganisationStatus':status[i],
                             'OrganisationClass':OrganisationClass,
                             'OrganisationSubClass':OrganisationSubClass,
                             'PrincipalActivity':'WHOLESALE DISTRIBUTOR' ,
                             'RecordStartDate': start_date[i].strftime("%Y-%m-%d"),#random.choice([fake_data.date_between_dates(date_start = start_date[i], date_end = start_date[i]+pd.DateOffset(years=1)).strftime("%Y-%m-%d"),start_date[i].strftime("%Y-%m-%d")]),
                             'RecordEndDate':end_date[i].strftime("%Y-%m-%d"),
                             'NameStartDate':reg_start_date,
                             'RegistrationStartDate':reg_start_date,
                             'RegistrationEndDate':end_date[i].strftime("%Y-%m-%d"),
                             'DeregnReasonCode':'',
                             'LastBalanceDate': '',
                             'StateOfIncorporation' : random.choice(stateOfInc),
                             'PlaceOfIncorporation' : placeOFInc,
                             'DisclosingEntityFlag':'',
                             'SuperTrustCoyFlag': '',
                             'PreviousStateofRegn':'',
                             'PreviousStateRegnNo':'',
                             'NextReviewDDMM'	:reg_start_date[-5:].replace('-',''),
                             'UpdateDocNo':'',
                             'CreateDocNo':'',
                             'AmendInd':'',
                             'FormerNameFlag':'',
                             'NextReviewDate':'',
                             'NonASICOrgFlag':'',
                             'OrgNameID' :'',
                             'SuccessorOrgNo':'',
                             'FirstBalanceDate':'',
                             'PreviousOrgSubClass':'',
                             'CreationDate':'',
                             'OMD_OPERATOR_ID':''} for i in range(len(status))] 
            index=index+1
        
            fake_org_df = pd.DataFrame(fake_org)

            
            fake_org_df['RegistrationEndDate']= np.where(fake_org_df['OrganisationStatus'] == 'DREGD', fake_org_df['RecordStartDate'],'9999-12-31')
            fake_org_df['DeregnReasonCode']= np.where(fake_org_df['OrganisationStatus'] == 'DREGD', 'S601AA','')
            frame.append(fake_org_df)

    result =  pd.concat(frame,ignore_index=True)
    return result,index
def start_end_date(status):
    fake = Faker('en_AU')
    start = fake.date_between(start_date="-50y", end_date="today")
    start_dates = []
    end_dates = []
    start_dates.append(start)
    for i in range(len(status)):
        if i != len(status)-1:
            end = fake.date_between_dates(date_start=start_dates[i], date_end=None)
            end_dates.append(end)
            next_start = end + timedelta(days=1)
            start_dates.append(next_start)
        else:
           a = datetime.strptime('9999-12-31','%Y-%m-%d')
           end_dates.append(a)

    return start_dates,end_dates
def define_stat_class_subclass(org_type):
        status = []
        cl = []
        sub_cl = []
        
        if(org_type != ''):
            df= read_excel('TestDatav4.xlsx', 'Config')
            df1= df[['OrganisationType', 'OrganisationStatus','OrganisationClass' ,'OrganisationSubClass']]
            istrue=df1['OrganisationType']== org_type
            new_df = df1[istrue]
            stat = []
            org_class = []
            org_subclass = []
            for index, row in new_df.iterrows():
                orgType= row['OrganisationType'] 
                orgStat =row['OrganisationStatus']
                orgClass= row['OrganisationClass']
                orgsubClass= row['OrganisationSubClass']
                stat.append(orgStat)
                org_class.append(orgClass)
                org_subclass.append(orgsubClass)
                
                
                
            status=list(dict.fromkeys(stat))
            cl=list(dict.fromkeys(org_class))
            sub_cl=list(dict.fromkeys(org_subclass))
        
        
        return status,cl ,sub_cl 

def generate_unique_address(table):
    number = number_of_rows(table)
    addressno = list(range(100,100+number))
    return addressno
def get_acn(number):
    df= read_excel('TestDatav4.xlsx', 'acn-abn')
    acn_list = df['ACN'].values.tolist()
    final_acn_list =[]
    for i in range(number):
       final_acn_list.append(acn_list[i])
    return final_acn_list
    
def get_abn(number):
    df= read_excel('TestDatav4.xlsx', 'acn-abn')
    abn_list = df['ABN'].values.tolist()
    final_abn_list =[]
    for i in range(number):
       final_abn_list.append(abn_list[i])
    return final_abn_list

         
    


# print the tables for inserting data
tables=read_generated_tables('TestDatav4.xlsx','Main')
print(tables)
acn_type = []
org_acn =[]
person_no=[]



# print the data for table
for table in tables:
    
    
    print("table name is", table)
    number_of_row = number_of_rows(table)
    data = read_table('TestDatav4.xlsx',table)
    columns = get_columns('TestDatav4.xlsx',table)
    ls = generate_data(table,columns,number_of_row,tables,acn_type,org_acn,person_no)
    if table == 'OrgExportMBR':
        for item in ls:
                acn = item[0]
                orgtype = item[3]
                acn_type.append([acn,orgtype])
                org_acn.append(acn)
    if table == 'PersonExportMBR':
        for item in ls:
            no = item[0]
            person_no.append(no)
            

   
    lst_tuple =list(map(tuple, ls))
    q = insert_data(table,columns)
  
    print('The number of rows are:',number_of_row)
    print(q)
    print(lst_tuple)
   
    with open(table+'.txt' , 'w') as f:
         f.write(q+"\n")
         f.write("\n")
         for l in lst_tuple :
           f.write(str(l) +"\n")




