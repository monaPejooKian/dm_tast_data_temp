# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 12:12:38 2021

@author: mona.abedi
"""
import pyodbc
import numpy as np
import pandas as pd
from pandas import read_excel
import random
from random import randint
import string

class AddressExportMBR:

    def __init__(self,ISN,add_no):
       suburb=['Essondon','Keilor','Moonee','Mill park', 'St Kilda']
       state =['VIC','NSW','ACT','NT','QLD','SA']
       
       self.ISN=ISN
       self.AddressNo= add_no
       self.AddressLine1 = self.random_string_generator_addressLine1(5,string.ascii_letters+string.digits)
       self.AddressLine2 = random.choice(suburb)
       self.StreetNo	= randint(1,999)
       self.StreetType= ''	
       self.StreetName	= self.random_string_generator_addressLine1(5,string.ascii_letters+string.digits)
       self.Locality = ''
       self.PostCode	= random.randint(1000,9999)
       self.State =	 random.choice(state)
       self.Country	='Australia'
       self.AmendIndicator	= ''
       self.UpdateDocNo	= ''
       self.CreatedDocNo = ''
       self.DPID	= ''
       self.Form379AddressNo= ''
       self.CreatedDate	= ''
       self.ForeignAddressLine1 = ''
       self.ForeignAddressLine2 = ''
       self.ForeignAddressLine3 = ''
       self.ForeignAddressLine4 = ''
       self.OMD_INSERT_DATETIME = ''
       self.OMD_OPERATOR_ID = ''
     
    
    def get_data(self):
        l=[ self.ISN,
            self.AddressNo,
            self.AddressLine1,
            self.AddressLine2,self.StreetNo,
            self.StreetType,
            self.StreetName,
            self.Locality,
            self.PostCode,
            self.State ,
            self.Country	,
            self.AmendIndicator,
            self.UpdateDocNo,
            self.CreatedDocNo,
            self.DPID,
            self.Form379AddressNo,
            self.CreatedDate,
            self.ForeignAddressLine1 ,
            self.ForeignAddressLine2 ,
            self.ForeignAddressLine3 ,
            self.ForeignAddressLine4,
            self.OMD_INSERT_DATETIME,
            self.OMD_OPERATOR_ID ]
        return l
    
    def random_string_generator_addressLine1(self,str_size, allowed_chars):
        s=''.join(random.choice(allowed_chars) for x in range(str_size))
        new_string=s +' '+ 'st'
        return new_string
    def random_string_generator_addressLine2(self,str_size, allowed_chars):
        s=''.join(random.choice(allowed_chars) for x in range(str_size))
        new_string=s +' '
        return new_string
    def get_addressno(self):
        return self.AddressNo
    
    
    
    
    
    
    
    